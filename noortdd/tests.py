from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, register_profile, profil, books, get_books
from .models import Status, Profile
from .forms import Status_Form, Profile_Form


import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
'''
class Story6FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable') 
		chrome_options.add_argument('--no-sandbox') 
		chrome_options.add_argument('--headless') 
		chrome_options.add_argument('disable-gpu') 
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		#self.browser.implicitly_wait(25)
		super(Story6FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit() 
		super(Story6FunctionalTest, self).tearDown()


	# test input

	def test_input_status(self):
		browser = self.browser
		#time.sleep(3)
		browser.get('http://noor-story6.herokuapp.com/') #    http://localhost:8000/
		time.sleep(3)
		status = browser.find_element_by_id("statusid")
		string = 'test status'
		status.send_keys(string)
		time.sleep(3)
		submit =  browser.find_element_by_id('submitid')
		submit.send_keys(Keys.RETURN)
		time.sleep(3)
		self.assertIn(string,browser.page_source)


	# test isinya

	def test_home_string_exist(self):
		browser = self.browser
		browser.get('http://noor-story6.herokuapp.com/')
		time.sleep(3)
		#homebtnstr = browser.find_element_by_id('homebtn').text
		string = 'Home'
		self.assertIn(string, browser.page_source)


	def test_intro_string_exist_at_the_introid(self):
		browser = self.browser
		browser.get('http://noor-story6.herokuapp.com/')
		time.sleep(3)
		intro = browser.find_element_by_id('introid').text
		string = 'Hello, Apa kabar?'
		self.assertEqual(string, intro)


	# test css

	def test_navbar_bg_color(self):
		browser = self.browser
		browser.get('http://noor-story6.herokuapp.com/')
		time.sleep(3)
		navbar = browser.find_element_by_id('navbarid')
		navbarbgcolor = navbar.value_of_css_property('background-color')
		self.assertEqual('rgba(52, 58, 64, 1)', navbarbgcolor)

	def test_submit_button_has_padding(self):
		browser = self.browser
		browser.get('http://noor-story6.herokuapp.com/')
		time.sleep(3)
		submit = browser.find_element_by_id('submitid')
		self.assertEqual('1px 6px', submit.value_of_css_property('padding'))
'''

class Story6UnitTest(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
	
	def test_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_using_landing_page_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'landing_page.html')

	'''
	def tes_status_form_validation_for_exceeding_length(self):
		status_form = Status_Form(data={'status':
		 '1jpUfYokancfBChztP6Gx8MzWO3VVFOR2jMbsfycebM36hRzMvxKwYLJm7ga7iiettcKOCh6xqtzOg2dtPyQBHdsuXeAW3iAy4NNakxFcRiyan0mzw4LYaajO6uYjU9Ryl9gsfDFjrrS23CeBt6szAqdmbmcT7d3r5vmbrIz7BZSS4BajNOQb5yAjOOOrzXmv0VIz6fEcdvmuxf3xDNoy5QKefGBl1h8VQ1ixTO43RFqaQfDw2Wcorzf09PPtNo8S7LTXSzyqCczXWxXCHEE1s9Thh0e3UlhdeityF8M0Unqh'
		})
		self.assertFalse(status_form.is_valid())
		self.assertEqual(
			status_form.errors['status'],['Cant exceed 300 characters']
		)
	'''

	def test_model_can_create_status(self):
		status = Status.objects.create(status='status')

		counting_all_status = Status.objects.all().count()
		self.assertEqual(counting_all_status, 1)

	def test_status_validation_for_blank_items(self):
		form = Status_Form(data={'status': '','dates':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['status'],
			['This field is required.']
		)

	def test_status_post_success_and_render_the_result(self):
		test = 'Hello, Apa kabar?'
		response_post = Client().post('/save_status/', {'title': test, 'content': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)



	def test_status_post_error_and_render_the_result(self):
		test = 'Hello, Apa kabar?'
		response_post = Client().post('/save_status/', {'title': '', 'content':''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	########################################################################################

	def test_url_is_exist(self):
		response = Client().get('/register/')
		self.assertEqual(response.status_code, 200)
	
	def test_using_profile_func(self):
		found = resolve('/register/')
		self.assertEqual(found.func, register_profile)

	def test_using_landing_page_template(self):
		response = Client().get('/register/')
		self.assertTemplateUsed(response, 'register_profile.html')



	def test_model_can_create_profile(self):
		profile = Profile.objects.create(name='a',dob='2018-01-01',npm='0000000000')

		counting_all_profile = Profile.objects.all().count()
		self.assertEqual(counting_all_profile, 1)


	def test_profile_validation_for_blank_items(self):
		form = Profile_Form(data={'name': '','bod':'','npm':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['name'],
			['This field is required.']
		)
		self.assertEqual(
			form.errors['dob'],
			['This field is required.']
		)
		self.assertEqual(
			form.errors['npm'],
			['This field is required.']
		)

	def test_profile_post_success(self):
		test = 'Hello! Salam kenal'
		response_post = Client().post('/save_profile/', {'title': test, 'content': test})
		self.assertEqual(response_post.status_code, 302)


	def test_profile_post_error(self):
		test = 'Hello! Salam kenal'
		response_post = Client().post('/save_profile/', {'title': '', 'content':''})
		self.assertEqual(response_post.status_code, 302)

	def test_url_is_exist(self):
		response = Client().get('/profil/')
		self.assertEqual(response.status_code, 200)
	
	def test_story8_page_using_profil_func(self):
		found = resolve('/profil/')
		self.assertEqual(found.func, profil)

	def test_story8_page_using_template(self):
		response = Client().get('/profil/')
		self.assertTemplateUsed(response, 'profil.html')


	# story 9
	def test_books_url_exist(self):
		response = Client().get('/books/')
		self.assertEqual(response.status_code, 200)

	def test_books_books_page_using_func(self):
		page = resolve('/books/')
		self.assertEqual(page.func, books)

	def test_books_page_using_template(self):
		response = Client().get('/books/')
		self.assertTemplateUsed(response, 'books.html')
'''
class Story9FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable') 
		chrome_options.add_argument('--no-sandbox') 
		chrome_options.add_argument('--headless') 
		chrome_options.add_argument('disable-gpu') 
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		#self.browser.implicitly_wait(25)
		super(Story9FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit() 
		super(Story9FunctionalTest, self).tearDown()
'''	
'''
	def test_books_using_correct_database(self):
		test = "Quilting 101"

		browser = self.browser
		browser.get('http://127.0.0.1:8000/books/')
		#response= Client().get('/books/')
		time.sleep(3)
		#print(browser.page_source)
		button = browser.find_element_by_id('get_books_btn')
		button.click()
		time.sleep(10)

		self.assertIn(test, browser.page_source)
	def test_input_status(self):
		browser = self.browser
		#time.sleep(3)
		browser.get('http://noor-story6.herokuapp.com/') #    http://localhost:8000/
		time.sleep(3)
		status = browser.find_element_by_id("statusid")
		string = 'test status'
		status.send_keys(string)
		time.sleep(3)
		submit =  browser.find_element_by_id('submitid')
		submit.send_keys(Keys.RETURN)
		time.sleep(3)
		self.assertIn(string,browser.page_source)
		'''