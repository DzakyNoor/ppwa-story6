from django.db import models
from django.utils import timezone
# from .forms import Status_Form

# Create your models here.
class Status(models.Model):
	date = models.DateTimeField(default=timezone.now())
	status = models.CharField(max_length=300)

class Profile(models.Model):
	
	name = models.CharField(max_length=30)
	dob = models.DateField(default='')
	npm = models.CharField(max_length=10)

class Register(models.Model):
	name = models.CharField(max_length=100)
	email = models.CharField(max_length=100, unique = True)
	password = models.CharField(max_length=20)