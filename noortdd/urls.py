from django.urls import re_path
from django.urls import path
from .views import index,save_status,register_profile, save_profile, clear_status, profil, books, get_books, search_books, register_ajax, register_ajax_create, check_mail, oauth

urlpatterns = [
	path('',index, name='index'),
	path('save_status/',save_status,name='save_status'),
	path('register/',register_profile,name='register_profile'),
	path('save_profile/', save_profile, name='save_profile'),
	path('clear_status/', clear_status, name='clear_status'),

	# story 8
	path('profil/',profil,name='profil'),
	
	# story 9
	path('books/', books, name='books'),
	path('get_books/', get_books, name='get_books'),
	path('search_books/', search_books, name='get_books'),

	# story 10
	path('register_ajax/', register_ajax, name='register_ajax'),
	path('register_ajax/create/', register_ajax_create, name='register_ajax_create'),
	path('register_ajax/checkmail/', check_mail, name='check_mail'),

	# story 11
	path('oauth/', oauth, name='oauth'),

]