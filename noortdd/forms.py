from django import forms
#from .models import Status

class Status_Form(forms.Form):
	error_messages = {
		'required': 'Please type',
	}

	status_attrs = {
		'type': 'text',
		'class': 'todo-form-input',
		'placeholder': 'status',
		'id': 'statusid'
	}


	status = forms.CharField(label='Status', required=True, max_length=67, widget=forms.TextInput(attrs=status_attrs))

class Profile_Form(forms.Form):
	error_messages = {
		'required': 'Please type',
	}

	char_attrs = {
		'class': 'form-control',
	}

	num_attrs = {
		'class': 'form-control',
		'type': 'number',
	}

	date_attrs = {
		'class': 'form-control',
		'type': 'date',
	}

	name = forms.CharField(label='Name', required=True, max_length=30, widget=forms.TextInput(attrs=char_attrs))
	dob = forms.CharField(label='Date of Birth', required=True, widget=forms.DateInput(attrs=date_attrs))
	npm = forms.CharField(label='NPM', required=True, max_length=10, widget=forms.TextInput(attrs=num_attrs))

class Register_Ajax(forms.Form):
	error_messages = {
		'required': 'Please type',
	}

	text_attrs = {
		'class': 'form-control',
	}

	num_attrs = {
		'class': 'form-control',
		'type': 'number',
	}

	name = forms.CharField(label='Name', required=True, widget=forms.TextInput(attrs=text_attrs))
	email = forms.CharField(label='Email', required=True, widget=forms.EmailInput(attrs=text_attrs))
	password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput(attrs=text_attrs))