from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import Status, Profile, Register
from .forms import Status_Form, Profile_Form, Register_Ajax
import json
import requests

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

response = {}

# Create your views here.
def index(request):
	all_status = Status.objects.all()
	response['all_status'] = all_status
	response['title'] = 'Add Status'
	response['add_status'] =  Status_Form()
	return render(request,'landing_page.html',response)

def save_status(request):
	form = Status_Form(request.POST or None)
	if(request.method=='POST' and form.is_valid()):
		response['status']= request.POST['status']
		The_Status= Status(
			status=response['status'],
		  
		)
		The_Status.save()

	return HttpResponseRedirect('/')

	
def register_profile(request):
	all_profile = Profile.objects.all()
	response['all_profile'] = all_profile
	response['title'] = 'Add Profile'
	response['add_profile'] =  Profile_Form()
	return render(request,'register_profile.html',response)

def save_profile(request):
	form = Profile_Form(request.POST or None)
	if(request.method=='POST' and form.is_valid()):
		response['name']= request.POST['name']
		response['dob']= request.POST['dob']
		response['npm']= request.POST['npm']
		The_Profile= Profile(
			name=response['name'],
			dob=response['dob'],
			npm=response['npm'],
		)
		The_Status.save()

	return HttpResponseRedirect('/profile/')

def clear_status(response):
	Status.objects.all().delete()
	return HttpResponseRedirect('/')


# story 8
def profil(request):
	return render(request,'profil.html')


# story 9
def books(request):
	return render(request, 'books.html')

def get_books(request):
	print('test masuk')
	url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
	book = requests.get(url).json()
	return JsonResponse(book)

def search_books(request):
	print('masuk search')
	print(request)
	#temp = request.objects.get(data)
	temp = request.GET.get('inputStr','')

	print('temp pertama ' + temp)
	temp = 'https://www.googleapis.com/books/v1/volumes?q=' + temp
	
	print('temp kedua ' + temp)
	book = requests.get(temp).json()
	return JsonResponse(book)

# stoyy 10
def register_ajax(request):
	return render(request, 'register_ajax.html', {'Form' : Register_Ajax})


def register_ajax_create(request):
	try:
		if request.method == 'POST':
			name = request.POST['name']
			email = request.POST['email']
			password = request.POST['password']

			Register.objects.create(
				name = name,
				email = email,
				password = password
			)
		return HttpResponse('SUCCESS')

	except Exception as e:
		return HttpResponse('FAIL')


def check_mail(request):
	print(request)
	temp = request.GET['email']
	subscriber = Register.objects.all()
	print(subscriber)
	for data in subscriber:
		if(temp == data.email):
			return HttpResponse("")
	return HttpResponse("OK")

# story 11
def oauth(request):
	return render(request, 'oauth.html')
	
def register_user(request):
	name = request.POST['name']
	email = request.POST['email']
	password = request.POST['password']

	user = User.objects.create_user(name, email, password)

	return HttpResponse('')

def log_in(request):
	name = request.POST['name']
	password = request.POST['password']
	user = authenticate(request, username=username, password=password)

	if user is not None:
		login(request, user)
		return HttpResponse('Logged in')

	else:
		return HttpResponse('No ID found')


def log_out(request):
	logout(request)
	return HttpResponse('Logged out')