# Generated by Django 2.1.1 on 2018-10-11 06:14

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('noortdd', '0003_auto_20181011_1514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='dob',
            field=models.DateField(default=''),
        ),
        migrations.AlterField(
            model_name='status',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 10, 11, 6, 14, 38, 904733, tzinfo=utc)),
        ),
    ]
