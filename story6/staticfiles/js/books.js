var favourite = 0;

function changeStar(id){
    var test = id;
    console.log(test);
    var star = $('#'+id).css("color");
    console.log(star);
    if(star == 'rgb(33, 37, 41)'){
        console.log('in');
        favourite++;
        $('#fav').html(favourite);
        $('#'+id).css('color','orange');
    }

    else{
        favourite--;
        $('#fav').html(favourite);
        $('#'+id).css('color','#212529');
    }
}



        // changing style
        /*
        $('#'+id+'_num').css('background-color','');
        $('#'+id+'_num').css('color','#212529');
        $('#'+id+'_name').css('background-color','');
        $('#'+id+'_name').css('color','#212529');
        $('#'+id+'_str').css('background-color','');
        */

        // changing style
        /*
        $('#'+id+'_num').css('background-color','black');
        $('#'+id+'_num').css('color','white');
        $('#'+id+'_name').css('background-color','black');
        $('#'+id+'_name').css('color','white');
        $('#'+id+'_str').css('background-color','black');
        */


$(document).ready(function(){
    $("#get_books_btn").click(function(){
        console.log('pre if func');

        // ngecek tombol udah diklik (?)
        var style = $('#books_header').css('display');
        if(style == "block"){
            console.log('if func true');
            $("#get_books_btn").hide();
            $("#books_header").hide();
            $("#loader").show();
            console.log("out")

            $.ajax({
                url: "/get_books/",
                dataType : "json",

                success: function(response){   
                    $('#lists').empty();       
                    var w = "";
                    response = response["items"];
                    for (var i = 0; i < response.length; i++) {
                        var title = response[i]["volumeInfo"]["title"];
                        var publisher = response[i]["volumeInfo"]["publisher"];
                        var cover_src =response[i]["volumeInfo"]["imageLinks"]["thumbnail"];
                        var id_buku =response[i]["id"];

                        var desc_str = response[i]["volumeInfo"]["description"];

                        var desc;
                        if(desc_str.length > 200){
                            desc = desc_str.substring(0,198) + "...";
                        }
                        else{
                            desc = desc_str;
                        }

                        var cover = "<img src='"+cover_src+"' style='width:100px'>";
                        var stars = "<div class='fa fa-star fa-lg' id='"+id_buku+"' style='margin:50%' onclick='changeStar("+"\""+id_buku+"\""+")'></div>"
                        /*var stars = "<img id="+id_buku+" src='http://aux2.iconspalace.com/uploads/star-vector-icon-256-855333183.png'"+
                        " style='width:20px;margin-left: auto; margin-right: auto;' onclick='changeStar("+"\""+id_buku+"\""+")'>"
                        */

                        w = "";
                        w ="<tr><th scope='row' id='"+id_buku+"_num'>"+(i+1)+"</th>"+
                        "<td id='"+id_buku+"_name'>"+title+"</td>"+
                        "<td id='"+id_buku+"_desc'>"+desc+"</td>"+
                        "<td id='"+id_buku+"_cover'>" + cover + "</td>" +
                        "<td id='"+id_buku+"_str'>"+ stars + "</td></tr>";

                        $('#lists').append(w);
                    }

                    $("#loader").hide();
                    $("#books_menu").show()
                }
            });
        }
    });
})

function findBooks(){
    console.log('masuk func')

    favourite = 0;
    $('#fav').html(favourite);

    $('#loader_books_search').show();
    $('#books_table').hide();

    var input;
    input = $('#search_box').val();
    var newInput = input.split(" ");

    var query = newInput[0];
    for(var i=1; i<newInput.length; i++){
        query += "_" + newInput[i];
    }

    console.log(query);
    $.ajax({
        //type: "GET",
        url: "/search_books/",
        data: {inputStr:query},
        success: function(response){
            console.log(response);
            $('#lists').empty();

            var w = "";
            response = response["items"];
            for (var i = 0; i < response.length; i++) {
                var title = response[i]["volumeInfo"]["title"];
                var publisher = response[i]["volumeInfo"]["publisher"];
                var cover_src =response[i]["volumeInfo"]["imageLinks"]["thumbnail"];
                var id_buku =response[i]["id"];

                var desc_str = response[i]["volumeInfo"]["description"];

                var desc;

                try{
                    if(desc_str.length > 200){
                        desc = desc_str.substring(0,198) + "...";
                    }
                    else{
                        desc = desc_str;
                    }
                }
                catch(Err){
                    desc = 'No description';
                }


                var cover = "<img src='"+cover_src+"' style='width:100px'>";
                var stars = "<div class='fa fa-star fa-lg' id='"+id_buku+"' style='margin:50%' onclick='changeStar("+"\""+id_buku+"\""+")'></div>"
                /*var stars = "<img id="+id_buku+" src='http://aux2.iconspalace.com/uploads/star-vector-icon-256-855333183.png'"+
                " style='width:20px;margin-left: auto; margin-right: auto;' onclick='changeStar("+"\""+id_buku+"\""+")'>"
                */

                w = "";
                w ="<tr><th scope='row' id='"+id_buku+"_num'>"+(i+1)+"</th>"+
                "<td id='"+id_buku+"_name'>"+title+"</td>"+
                "<td id='"+id_buku+"_desc'>"+desc+"</td>"+
                "<td id='"+id_buku+"_cover'>" + cover + "</td>" +
                "<td id='"+id_buku+"_str'>"+ stars + "</td></tr>";

                $('#lists').append(w);
            }
            $('#loader_books_search').hide();
            $('#books_table').show();
        }
    })


    /*$.ajax({
        type: "POST",
        url: "http://127.0.0.1:8000/books/",
        dataType: "json",
        async: true,
        data:{
            csrfmiddlewaretoken: '{{ csrf_token }}',
            searchBox: $("#search_box").val()
        },

        success: function(json){
            console.log('masuk')
        }
    })*/
}
